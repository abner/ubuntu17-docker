FROM ubuntu:17.10
COPY sources.list /etc/apt/sources.list
RUN apt-get update && apt-get install -y locales && rm -rf /var/lib/apt/lists/* \
  && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8

RUN export uid=1000 gid=1000 && \
  mkdir -p /home/developer && \
  echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
  echo "developer:x:${uid}:" >> /etc/group && \
  usermod -aG sudo developer && \
  chown ${uid}:${gid} -R /home/developer



#RUN apt-cache search vim

#RUN rm -rf /var/lib/apt/lists/*

#USER developer
# ENV HOME=/home/developer